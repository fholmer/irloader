.PHONY: all clean build upload upgrade-venv freeze-venv restore-venv
 
all:
	@echo "  clean:"
	@echo "      remove pycache, dist/, build/ *.egg-info"
	@echo "      "
	@echo "  build:"
	@echo "      build sdist and bdist_wheel package"
	@echo "      "
	@echo "  upgrade-venv:"
	@echo "      update all packages"
	@echo "      "
	@echo "  restore-venv:"
	@echo "      update all packages to version in requirements-freeze"
	@echo "      "

clean:
	find . -maxdepth 2 -name '__pycache__' -exec rm --recursive --force {} +
	find . -maxdepth 2 -iname '*.pyc' -exec rm --force {} +
	find . -maxdepth 2 -iname '*.pyo' -exec rm --force {} +
	rm --force --recursive build/
	rm --force --recursive dist/
	rm --force --recursive *.egg-info

build:
	python setup.py sdist
	python setup.py bdist_wheel

upload:
	python -m twine upload dist/*

upgrade-venv:
	python -m pip install -U pip
	python -m pip install -U -r requirements-dev.txt
	python -m pip install -U -r requirements.txt

freeze-venv:
	python -m pip freeze > requirements-freeze.txt

restore-venv:
	python -m pip install -U requirements-freeze.txt

analyze-ir:
	jupyter notebook analyze_ir.ipynb
