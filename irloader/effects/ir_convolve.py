import numpy
import time

SAMPLE_LEN = 2**10
IR_LEN = 2**12

def file_interface(ir_data, source_data):
    loop = stream_interface(ir_data)
    loop.send(None)
    for start in range(0, len(source_data), SAMPLE_LEN):
        res = loop.send(source_data[start:start + SAMPLE_LEN])
        yield res

def stream_interface(ir_data):
    source_data = yield None
    assert not source_data is None

    irslice = ir_data[:IR_LEN]

    data_buffer = []
    # find signal buffer size
    b_size = SAMPLE_LEN // len(source_data)
    if b_size > 1:
        for _ in range(b_size):
            data_buffer.append(source_data)
    else:
        b_size = 1
        data_buffer.append(source_data)

    offset = sum(len(prev_data) for prev_data in data_buffer)

    #init signal
    data_buffer.append(source_data)
    assert len(data_buffer) == b_size + 1
    signal = numpy.concatenate(data_buffer)

    assert len(signal) > SAMPLE_LEN

    print(
        "buffer length: {}. data length: {}. buffer offset: {}.".format(
            len(signal),
            len(source_data),
            offset
        )
    )

    #t_count = 1

    while 1:

        conv = numpy.convolve(signal, irslice, mode="full")
        res = conv[offset:offset + len(source_data)]

        #if t_count == 0:
        #    t_diff = time.time() - t_prev
        #    print("ms: {}".format(t_diff * 1000))

        #t_count += 1
        #if t_count > 51:
        #    t_count = 0

        source_data = yield res

        #if t_count == 0:
        #    t_prev = time.time()

        del data_buffer[0]
        offset = sum(len(prev_data) for prev_data in data_buffer)
        data_buffer.append(source_data)

        signal = numpy.concatenate(data_buffer)
