from .settings import get_argument_parser, get_settings
from .irloader import IRLoader
import sounddevice

def main():

    args = get_argument_parser()

    if 'help' in args:
        return

    settings = get_settings(args)

    irloader = IRLoader(
        ir=settings.ir_file,
        source=settings.looper_file if settings.looper else '',
        target='',
        looper=settings.looper
    )

    irloader.read()
    irloader.process()
    
    if settings.looper:
        irloader.looper()
    else:
        irloader.play()
