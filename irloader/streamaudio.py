import sounddevice
from .effects import ir_convolve
import numpy

class StreamAudio():
    def __init__(self):
        self.info = sounddevice.default.samplerate
        sounddevice.default.channels = 1, 1
    
    def setup(self, ir_data, ir_samplerate):
        self.info = ir_samplerate
        sounddevice.default.samplerate = self.info
        self.ir_data = ir_data

    def play(self):
        convolve_interface = ir_convolve.stream_interface(self.ir_data)
        convolve_interface.send(None)

        def callback(indata, outdata, frames, time_, status):
            if status:
                print(status)
            #indata = next(loopy)
            sig = indata[:,0]
            sig = sig * 3
            res = convolve_interface.send(sig)
            if numpy.max(numpy.abs(res)) > 0.9:
                print("HIGH")
            outdata[:,0] = res

        try:
            with sounddevice.Stream(channels=1, latency=0.02, dtype='float32', callback=callback):
                while 1:
                    sounddevice.sleep(1000)
        except KeyboardInterrupt:
            pass


    def looper(self, source_data):
        def Loopy():
            offset = yield None
            assert offset > 1

            start = 0
            while 1:
                dataslice = source_data[start:start + offset]
                start += offset
                if len(dataslice) == offset:
                    offset = yield dataslice
                else:
                    print("restart")
                    start = 0
    
        loopy = Loopy()
        assert loopy.send(None) is None

        convolve_interface = ir_convolve.stream_interface(self.ir_data)
        convolve_interface.send(None)

        def callback(indata, outdata, frames, time_, status):
            if status:
                print(status)
            sig = loopy.send(frames)
            res = convolve_interface.send(sig)
            #if numpy.max(numpy.abs(res)) > 0.9:
            #    print("HIGH")
            outdata[:,0] = res

        try:
            #blocksize=1024
            with sounddevice.Stream(channels=1, latency=0.02, dtype='float32', callback=callback):
                while 1:
                    sounddevice.sleep(1000)
        except KeyboardInterrupt:
            pass
